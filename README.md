## Tombola

Progetto per il primo anno di Matematica Applicata UniVr.

Quanto segue è un'implementazione in Java del gioco della Tombola.

Lo scopo del progetto è mostrare l'utilizzo della programmazione ad oggetti per la creazione del gioco della Tombola.
In questo progetto si utilizza un file csv come database di controllo delle schede e del codice di controllo.

---

## Settings
Le schede vengono salvate in un file csv (file ASCII con estensione .csv), ogni scheda su una riga diversa. Ogni riga contiene 16 campi separati da una virgola: i 15 numeri della scheda e una stringa di controllo che serve a certificare che ogni scheda è autentica.
Il file csv prende il nome di schede.csv e si trova all'interno di una cartella File.

## Compilazione dei sorgenti

`javac Main.java Gioco.java Scheda.java ReadWrite_csv.java`

## Esecuzione

Spiegare come va lanciatoil programma. Anzi, secondo me a fare bene i programmi dovevano essere almeno 2: dovrebbe esserci il programma che gestisce la vendita delle schedine, che non è detto avvenga tutta in un singolo momento. Questo proramma sarebbe da lanciare quando si vende una o più schede. Pertanto esso deve poter partire da un file CSV che ha già delle righe. Gli serve inoltre un seed per l'avvio del generatore pseudo-casuale che va impostato quando ci si accinge a vendere la prima scheda. Quando avviato con un file CSV non vuoto il programma deve recuperare il seed da un file apposito e rigenerare internamente tutte le schede fino a quel punto (puù anche controllare che vengano uguali a quelle nel CSV) giundi procedere con la generazione di nuove schede, come richieste, e aggiungerle in fondo al file CSV.
E' meglio che il programma che il programma che genera le schede possa gestire avvii multipli, in un uso realistico è importante e nemmeno si potrebbe rischiare altrimenti (metti che ti si impalla la macchina dopo che hai venduto metà schede).

## Lista comandi

Per avviare il gioco si deve inserire il numero dei giocatori presenti e successivamente premere invio per ogni estrazione.

---

## Gioco

Le impostazioni prevedono:

1. Schede da 15 numeri scritti in riga senza ripetizione
2. Estrazioni di numeri casuali, non ripetuti
3. Numeri estratti e numeri delle schede sono da 1 a 90
4. Ogni volta che il numero estratto è presente sulla scheda viene memorizzato dal programma
5. Una volta memorizzato dal programma il punteggio di ogni singolo giocatore, automaticamente restituisce: ambo se sono stati trovati due numeri, terna se sono stati trovati tre numeri, quaterna se sono stati trovati quattro numeri, cinquina se sono stati trovati cinque numeri  
6. Lo scopo del gioco è quello di realizzare la tombola, ovvero arrivare per primi coprire tutti i numeri presenti nella scheda  


Il gioco può essere giocato da un numero qualunque di giocatori.
