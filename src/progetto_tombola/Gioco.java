package progetto_tombola;

import java.util.Random;



public class Gioco {
	
	int num_estratto;
	
	public Gioco(){}
	
	public int estrazione() 
	{
		Random rdn = new Random();
		num_estratto = rdn.nextInt(91);
		return num_estratto;
	}
	
	public int get_nrEstratto()
	{
		return num_estratto;
	}
	
	public String toString() 
	{
		
		return "Estratto il numero: " + estrazione();
	}
	
	
}