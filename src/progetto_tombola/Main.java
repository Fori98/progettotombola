package progetto_tombola;

import java.io.ObjectInputStream.GetField;
import java.util.*;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int nr_giocatori;
		
		System.out.print("Inserire il numero di giocatori: ");		//Seleziono il numero di giocatori
		nr_giocatori = sc.nextInt();
	
		
		//Assegno le schede ai giocatori e le salvo sul file csv
		Scheda schede[] = new Scheda[nr_giocatori];
		ReadWrite_csv read_write = new ReadWrite_csv(nr_giocatori);
	
		for (int i = 0; i < nr_giocatori; i++)		//Creo le schede
		{
			Scheda scheda = new Scheda();		
			schede[i] = scheda;
		}
		read_write.write_csv(schede);		// Le scrivo su file
	
		// Inizio gioco
		Gioco gioco = new Gioco();			//creo nuovo oggetto gioco
		
		while(read_write.get_Punteggio() != 1)		//punteggi per le vincite
		{
			
			read_write.read_csv(gioco.estrazione());
			System.out.print("Numero estratto: " + gioco.get_nrEstratto() + "\n");
			
			if (read_write.get_Punteggio() == 5)
			{
				System.out.print("Ambo della scheda nr " + read_write.get_posVincente() + "\n");
			}
			else if (read_write.get_Punteggio() == 4)
			{
				System.out.print("Terna della scheda nr " + read_write.get_posVincente() + "\n");
			}
			else if (read_write.get_Punteggio() == 3)
			{
				System.out.print("Quaterna della scheda nr " + read_write.get_posVincente() + "\n");
			}
			else if (read_write.get_Punteggio() == 2)
			{
				System.out.print("Cinquina della scheda nr " + read_write.get_posVincente() + "\n");
			}
			
			System.out.print("Premi invio per estrarre un nuovo numero\n");	
			sc.nextLine();		
		}
		System.out.print("Tombola della scheda nr " + read_write.get_posVincente() + "\n");
		
	}

}
