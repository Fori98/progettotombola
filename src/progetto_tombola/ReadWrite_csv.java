package progetto_tombola;
import progetto_tombola.Scheda;
import java.awt.List;
import java.io.*;
import java.util.ArrayList;
import java.lang.reflect.Array;
import java.util.Arrays;

public class ReadWrite_csv{
	int righe;
	int punti[];
	int punteggio;
	int pos_vincente;
	
	//Costruttore
	public ReadWrite_csv(int nr_giocatori)
	{
		righe = nr_giocatori;
		punti = new int[righe];
	}
	
	//Scrive il file
	public void write_csv(Scheda[] schede)
	{
		try (FileWriter writer = new FileWriter("file/schede.csv")){
			
			for (int i = 0; i < schede.length ; i++)
			{
				Scheda sc = schede[i];			
				int[]values = sc.getNr_scheda();
				int len = sc.getNr_scheda().length;
				String controllo = sc.getNr_controllo();
				
				for (int j = 0; j < len; j++) 
				{
					writer.append("" + values[j]);
					writer.append(",");
		        }
				
				writer.append(controllo);
				writer.append("\n");
				
			}
			
			writer.flush();
		}
		catch (Exception e) {
			System.out.print("ERRORE! Scrittura file");
		}
	}
	
	//legge il file
	public void read_csv(int num_estratto)
	{
		
	   try ( BufferedReader reader = new BufferedReader(new FileReader("file/schede.csv")) )
	   {
		   String line = "";
		   int pos = 0;		//posizione
		   
		   while ( (line = reader.readLine()) != null )
		   {   
			   //line contiene una riga per volta
			   String schede[] = line.split(",");
			   for(int i = 0; i < 15; i++) 
			   {
				   if(num_estratto == Integer.parseInt(schede[i]))
				   {
					   punti[pos] += 1;
					  
				   }
			   }
			   pos ++;
			}
		   
		   punteggio = check_point(punti);		//controllo dei punti
		     
	   }
	   catch (Exception e) {
		// TODO: handle exception
	   }	   
	}	
	
	public int get_posVincente()
	{
		return pos_vincente;
	}
	
	public int get_Punteggio()
	{
		return punteggio;
	}
	
	public int check_point(int punti[])
	{
		for (int i = 0; i < punti.length; i++)
		{	
			switch (punti[i]) {
			case 15:
				pos_vincente = i;
				return 1;
			case 5:
				pos_vincente = i;
				return 2;
			case 4:
				pos_vincente = i;
				return 3;
			case 3:
				pos_vincente = i;
				return 4;
			case 2:
				pos_vincente = i;
				return 5;
			default:
				break;
			}
		}
		return 0;
	}
}