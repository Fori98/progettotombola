package progetto_tombola;
import java.util.Random;

/*
 * Classe che inizializza la scheda con 15 numeri casuali + 1 codice di controllo
 */
public class Scheda {
	
	//Attributi
	private int nr_scheda[] = new int[15];   //numero scheda(15 numeri casuali)
	private String nr_controllo = "";		 //numero di controllo(16esimo numero)
	
	// Costruttore
	public Scheda() 
	{
		Random random = new Random();
		
		for(int i = 0; i < 15; i ++) {				//creazione schede
			nr_scheda[i] = random.nextInt(91);	
		}
		
		for (int j = 0; j < 5; j++) {				//creazione numero di controllo
			nr_controllo += switch_letter(random.nextInt(26));		// Si appoggia ad una funzione privata per la trasformazione in lettere.
		}
	}

	public String toString() {
		String risultato="";
		for(int i=0; i<15; i++) {
			risultato += " " + nr_scheda[i];
		}
		return risultato;
	}
	
	public String getNr_controllo() 
	{
		return nr_controllo;
	}
	
	public int[] getNr_scheda()
	{
		return nr_scheda;
	}
	
	private String switch_letter(int value) 
	{
		switch(value) 
		{
		case 0:return "A";
		case 1:return "B";
		case 2:return "C";
		case 3:return "D";
		case 4:return "E";
		case 5:return "F";
		case 6:return "G";
		case 7:return "H";
		case 8:return "I";
		case 9:return "J";
		case 10:return "K";
		case 11:return "L";
		case 12:return "M";
		case 13:return "N";
		case 14:return "O";
		case 15:return "P";
		case 16:return "Q";
		case 17:return "R";
		case 18:return "S";
		case 19:return "T";
		case 20:return "U";
		case 21:return "V";
		case 22:return "W";
		case 23:return "X";
		case 24:return "Y";
		case 25:return "Z";
		default:return "ERROR, valore immesso non valido";
		}
	}
}

